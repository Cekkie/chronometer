﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Util;
using Android.Content.PM;

namespace Chronometer
{
	[Activity (Label = "@string/app_name", MainLauncher = true, Icon = "@drawable/icon", ConfigurationChanges = ConfigChanges.Locale | ConfigChanges.ScreenSize)]
	public class MainActivity : Activity
	{
		private Handler handler;
		private TextView timeview;
		private Button startButton;
		private Button resetButton; 
		private TimeSpan accumulatedTime;
		private ISharedPreferences data;
		private bool start = false;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Main);
			handler = new Handler ();
			data = this.GetSharedPreferences ("Chronometer", FileCreationMode.Private);
			// Set our view from the "main" layout resource
			accumulatedTime = new TimeSpan(0,0,0,0,0);
			timeview = (TextView)FindViewById (Resource.Id.timeview);
			startButton = (Button)FindViewById (Resource.Id.startStopButton);
			resetButton = (Button)FindViewById (Resource.Id.resetButton);
			start = false;
			startButton.Text = Resources.GetText (Resource.String.start);

			// Get our button from the layout resource,
			// and attach an event to it
			startButton.Click += (sender, EventArgs) =>  Increment();
			resetButton.Click += (sender, EventArgs) => Reset ();
			GenerateDelayedTick ();
			startButton.Text = Resources.GetText (Resource.String.start);
		}

		private void ShowCount(){
			timeview.Text = accumulatedTime.ToString ( "hh':'mm':'ss" );	
		}


		public void Increment(){
			if (start) {
				//STOP
				start = false;
				startButton.Text = Resources.GetText (Resource.String.start);
			} else {

				start = true;
				startButton.Text = Resources.GetText (Resource.String.stop);

				GenerateDelayedTick ();
			}
		}

		public void Reset(){
			accumulatedTime = new TimeSpan (0, 0, 0, 0, 0);
			startButton.Text = Resources.GetText (Resource.String.start);
			start = false;
			ShowCount ();
		}


	

		protected override void OnStart(){
			Resume ();
			base.OnStart ();
		}
		protected override void OnResume(){
			Resume ();
			base.OnResume ();
		}


		private void Resume(){
			start = data.GetBoolean ("start", false);
				startButton.Text = start ? Resources.GetText (Resource.String.start) : Resources.GetText (Resource.String.stop);
			accumulatedTime = TimeSpan.FromMilliseconds ((double)data.GetInt ("time", 0));
		}
		protected override void OnPause(){
			
			SaveInfo ();
			base.OnPause ();
		}

		private void SaveInfo(){
			ISharedPreferencesEditor editor = data.Edit ();
			editor.PutInt("time",(int) accumulatedTime.TotalMilliseconds);
			editor.PutBoolean ("start", start);
			editor.Commit();
		}
		protected override void OnStop(){
			base.OnStop ();
		}
		protected override void OnDestroy(){
			base.OnDestroy ();
		}

		private void GenerateDelayedTick(){
			ShowCount ();
			handler.PostDelayed(OnTick, 500);
				
		}

		private void OnTick(){
			if (start) {
				accumulatedTime = accumulatedTime.Add (new TimeSpan (0, 0, 0, 0, 500));
				ShowCount ();
				GenerateDelayedTick ();
			}
		}

	}
}


